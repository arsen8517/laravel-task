<!-- resources/views/messages.blade.php -->
<link rel="stylesheet" href="{{ asset('css/reset.css') }}">
<link rel="stylesheet" href="{{ asset('css/main.css') }}">

<div class="main-page">

    <div class="block-form">
        <form method="post" action="{{ route('messages.store') }}" class="form">
            @csrf
            <div class="input-field">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name" required>
            </div>
            <div class="input-field">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required>
            </div>
            <div class="input-field">
                <label for="message">Message:</label>
                <textarea id="message" name="message" required></textarea>
            </div>
            <button type="submit">Submit</button>
        </form>

        @if (session('success'))
            <div>
                {{ session('success') }}
            </div>
        @endif
    </div>

    <div class="block-messages">
        <h2>Messages:</h2>
        <ul>
            @foreach ($messages as $message)
                <li>
                    <strong>Name:</strong> {{ $message->name }} <br>
                    <strong>Email:</strong> {{ $message->email }} <br>
                    <strong>Message:</strong> {{ $message->message }} <br>
                    <strong>Submitted at:</strong> {{ $message->created_at }}
                </li>
            @endforeach
        </ul>
    </div>

</div>