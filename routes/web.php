<?php

use Illuminate\Support\Facades\Route;

// routes/web.php

use App\Http\Controllers\MessageController;

Route::get('/messages', [MessageController::class, 'index'])->name('messages.index');
Route::post('/messages/store', [MessageController::class, 'store'])->name('messages.store');

Route::get('/', function () {
    return view('welcome');
});
